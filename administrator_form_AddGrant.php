<?php
	error_reporting(E_ALL);
	ini_set('display_errors',1);

		//set timezone for correct timestamp		
		date_default_timezone_set('America/New_York');
		
		//input check
        if(empty($_POST['addGrantType'])){
					echo "Please select a grant type";
					echo "<a href=adminAdd.html>Click Return</a>";
					die();
				}
				
		if(empty($_POST['addID'])) {
                    echo 'Please enter an ID';
                    echo "<a href=adminAdd.html>Click Return</a>";
					die();
                }

        if (is_numeric($_POST['addID']) && is_int(0+$_POST['addID'])){
        }
        else{
                  echo 'Please enter a number';
                  echo "<a href=adminAdd.html>Click Return</a>";
                  die();
            }
				
		if(empty($_POST['addName'])) {
                   echo 'Please enter a name';
                   echo "<a href=adminAdd.html>Click Return</a>";
				   die();
                }
				
		if(empty($_POST['addAmount'])) {
                   echo 'Please enter an amount';
                   echo "<a href=adminAdd.html>Click Return</a>";
				   die();
                }
				
        else {      $_POST['addAmount'] = str_replace(".00", "", $_POST['addAmount']);
					$_POST['addAmount'] = str_replace(",", "", $_POST['addAmount']);
					$_POST['addAmount'] = str_replace("$", "", $_POST['addAmount']);
																						
		if (is_numeric($_POST['addAmount']) && is_int(0+$_POST['addAmount'])){
						
                }
				
				else {								
				echo 'Please enter a dollar amount';
                echo "<a href=adminAdd.html>Click Return</a>";
				die();							
				}}
				
	    if(empty($_POST['addNumber'])) {
                   echo 'Please enter a number';
                   echo "<a href=adminAdd.html>Click Return</a>";
				   die();
                }
				
       if (is_numeric($_POST['addNumber']) && is_int(0+$_POST['addNumber'])){
       } else{
                  echo 'Please enter a number';
                  echo "<a href=adminAdd.html>Click Return</a>";
				  die();
                }
		
				
		 if($_POST['addGrantType'] == "Faculty Grant"){
            $addGrantURL = "http://ctspacegrant.org/wp-content/uploads/2011/08/CSGC-faculty-booklet-spring-2015.pdf";  
        }
        else {
            $addGrantURL = "http://ctspacegrant.org/wp-content/uploads/2011/08/CSGC-student-booklet-spring-2015.pdf";
        }
  
       $grant = array(
									"GrantType"         =>    $_POST['addGrantType'],
									"GrantID" 			=>	  $_POST['addID'],
									"GrantName"         =>    $_POST['addName'],
									"GrantAmount" 	    =>    $_POST['addAmount'],
									"NumberOfGrants"    =>    $_POST['addNumber'],
									"url"               =>    "$addGrantURL",
									"Timestamp"     	=>    date('m-d-Y H:i:s'),
					   );

					$json = json_encode($grant, JSON_PRETTY_PRINT);

					printf("<pre><font size=8>%s</font></pre>", $json);			

					// php5-curl
                    $url = "ctspace.me/post/grant";                        
					$ch = curl_init($url);                                                                      
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
					curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                  
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
					// curl_setopt($ch, CURLOPT_PORT, 5001);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 100);      
                    curl_setopt($ch, CURLOPT_HEADER, 1); 
                    curl_setopt($ch, CURLINFO_HEADER_OUT, true);                                                              
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
					    'Content-Type: application/json',                                                                                
					    'Content-Length: ' . strlen($json))                                                                       
					);                                                                                                                   
				
					if(curl_exec($ch) === false){
                        echo '<font size=15><strong>DEBUG MODE</strong>  -  <a href=adminBadSubmit.html>What the user normally sees</a></font><br>';
                        echo '<font size=15>Error: ' . curl_error($ch) . '<br>';
                    }
                    else{    
                        echo '<font size=15><strong>DEBUG MODE</strong>  -  <a href=adminSubmit.html>What the user normally sees</a></font><br>';                                                          
                        echo '<font size=15>JSON successfully sent<br>'; 
					}
                    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    echo 'http code: ' . $httpcode .'<br>';
                    var_dump(curl_getinfo($ch,CURLINFO_HEADER_OUT)); 
                    echo '</font>';        														  		

        if(curl_exec($ch) === false){
               header('Location:adminBadSubmit.html');     
        }
        else{                                                              
            header('Location:adminSubmit.html');            
        }

    curl_close($ch);
        
?>