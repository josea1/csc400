from flask import Flask, render_template, url_for, redirect
from flask.ext.script import Manager
from flask.ext.bootstrap import Bootstrap
from flask.ext.moment import Moment
from flask.ext.wtf import Form
from wtforms import StringField, SubmitField, IntegerField, FloatField, SelectField, validators
from wtforms.validators import Required
import pymongo 
from pymongo import MongoClient  


#Database connection
server = "ds045021.mongolab.com"
port = 45021
db_name = "bitbucket"
username = "antespo"
password = "tonyespo123"

conn = MongoClient(server, port)
db = conn[db_name]
db.authenticate(username, password)

#flask
app = Flask(__name__)
app.config['SECRET_KEY'] = 'hard to guess string'

manager = Manager(app)
bootstrap = Bootstrap(app)
moment = Moment(app)


#Forms
class AppForm(Form):
    grantId = IntegerField('Grant ID')
    
    appId = IntegerField('Applicant ID')
    
    name = StringField('Name')
    
    school = StringField('School')

    major = StringField('Major')

    preAwards = StringField('Previous space grant awards')

    citizenship = StringField('Citizenship')

    time = StringField('Proposed Period or performance')

    research = StringField('Title of Research')
    
    advisor = StringField('Academic Advisor')
    
    budget = FloatField('Budget')

    submit = SubmitField('Submit')


class EvalForm(Form):
    grantId = IntegerField('Grant ID')

    appId = IntegerField('Application ID')
    
    abstractScore = IntegerField('Abstract score')
    
    goalsObjectivesScore = IntegerField('Goals and objectives score')

    evalNum = IntegerField('Evaluator Number')

    submit = SubmitField('Submit')


class GrantForm(Form):
    grantId = IntegerField('Grant ID')
    
    award = StringField('Award')
    
    awardAmount = IntegerField('Award Amount')

    numAwards = IntegerField('Number of Awards')

    url = StringField('URL')
    
    #type1 = StringField('Type')
    type1 = SelectField('Type', choices=[('Student','Student'),('Faculty','Faculty')])

    submit = SubmitField('Submit')
    

class EvalLookUpForm(Form):
    grantId = IntegerField('Grant ID')
    
    appId = IntegerField('Application ID')

    submit = SubmitField('Submit')
    

class EditAppForm(Form):
    grantId = IntegerField('Grant ID')
    
    appId = IntegerField('Application ID')

    field = SelectField('Field To Be Editted',coerce=str, choices=[(u"",""),('School','School'),('Name','Name'),('Time','Time'),('Research','Research'),('budget','Budget'),
                                                       ('citizenship','Citizenship'),('Major','Major'),('preAwards','Previous Awards'),('Advisor','Advisor')],validators=[validators.Optional()])
    edit = StringField('Edit')

    submit = SubmitField('Submit')
    

class DeleteAppForm(Form):
    grantId = IntegerField('Grant ID')
    
    appId = IntegerField('Application ID')

    submit = SubmitField('Submit')
    

class EditEvalForm(Form):
    grantId = IntegerField('Grant ID')
    
    appId = IntegerField('Application ID')

    evalNum = IntegerField('Evaluation Number')

    field = SelectField('Field To Be Editted', choices=[('abstractScore','Abstract score'),('goalsObjectivesScore','Goals and objectives score')])
    
    edit = StringField('Edit')

    submit = SubmitField('Submit')


class EditGrantForm(Form):
    grantId = IntegerField('Grant ID')

    field = SelectField('Field To Be Editted', choices=[('awardAmount','Award Amount'),('url','URL'),('numAwards','Number of Awards'),('award','Award'),('Type','Type')]) 

    edit = StringField('Edit')
    
    submit = SubmitField('Submit')

class DelAllAppForm(Form):
    grantId = IntegerField('Grant ID')

    submit = SubmitField('Submit')

#homepage
@app.route("/")
def home():
    return render_template('base.html')

#all raw data
@app.route("/All")
def allData():
    posts = db.posts
    return render_template("allData.html",posts=posts)

#Applicants form
@app.route('/Applicants', methods=['GET', 'POST'])
def Applicants():
    posts = db.posts
    
    grantId = None
    name = None
    school  = None
    major = None
    preAwards = None
    citizenship = None
    time = None
    research = None
    advisor = None
    budget = None
    abstractScore = None
    goalsObjectivesScore = None
    
    form = AppForm()
    if form.validate_on_submit():
        
        grantId = form.grantId.data
        name = form.name.data
        school  = form.school.data
        major  = form.major.data
        preAwards = form.preAwards.data
        citizenship = form.citizenship.data
        time = form.time.data
        research = form.research.data
        advisor = form.advisor.data
        budget = form.budget.data
        appId = form.appId.data

        if posts.update({"_id" :grantId },{"$push":{"Applicants" : {"appId" : appId ,"Name" : name,"School" : school,"Major" : major,"preAwards" :preAwards,
                                                                    "citizenship" : citizenship,"Time" : time,"Research" : research,"Advisor" : advisor,
                                                                    "budget" :budget,"URL" : "www.url.com"}}})['nModified'] == 0:
            return render_template('noGrant.html')
        else:
            return redirect(url_for('Applicants'))
        
    return render_template('index.html', form=form)

#evaluators form
@app.route('/Evaluators', methods=['GET', 'POST'])
def evaluators():
    posts = db.posts
    
    grantId = None
    abstractScore = None
    goalsObjectivesScore = None
    evalNum = None
    appId = None
    
    form = EvalForm()
    if form.validate_on_submit():
        
        grantId = form.grantId.data
        abstractScore = form.abstractScore.data
        goalsObjectivesScore = form.goalsObjectivesScore.data
        evalNum = form.evalNum.data
        appId = form.appId.data

        if posts.update({"_id":grantId,"Applicants.appId":appId},{"$push":{"Applicants.$.Evaluators":{"evalNum":evalNum, "abstractScore":abstractScore,
                                                                                                      "goalsObjectivesScore" : goalsObjectivesScore}}})['nModified'] == 0:
            return render_template('noApp.html')
        else:
            return redirect(url_for('evaluators'))

        
    return render_template('index.html', form=form)

#grant form
@app.route('/Grants', methods=['GET', 'POST']) 
def grants(): 
    posts = db.posts

    grantId = None
    award = None
    awardAmount = None
    numAwards = None
    url = None
    Type = None
    
    form = GrantForm()
    if form.validate_on_submit():

        grantId = form.grantId.data
        award = form.award.data
        awardAmount = form.awardAmount.data
        numAwards = form.numAwards.data
        url = form.url.data
        type1 = form.type1.data

        posts.insert({"_id" : grantId,"Type":type1.lower(),"award":award,"awardAmount":awardAmount,"numAwards":numAwards,"url":url})
        return redirect(url_for('grants'))
        
    return render_template('index.html', form=form)

#look up evaluations for Applicants
@app.route('/EvalLookUp', methods=['GET', 'POST']) 
def evalLookUp(): 
    posts = db.posts

    grantId = None
    appId = None

    
    form = EvalLookUpForm()
    if form.validate_on_submit():

        grantId = form.grantId.data
        appId = form.appId.data


        try :
            if posts.find({'_id' :grantId , "Applicants.appId" : appId}, { "Applicants.$.Evaluators" : 1 }).count() == 0:
                return render_template('noApp.html')
            for post in posts.find({'_id' :grantId , "Applicants.appId" : appId}, { "Applicants.$.Evaluators" : 1 }):
                for i in range(len(post["Applicants"][0]["Evaluators"])):      
                    print "--"
        except:
            return render_template('noEvalDone.html')
    return render_template('evalLookUp.html',posts=posts, form=form, grantId=grantId, appId=appId)

#edit applications
@app.route('/EditApp', methods=['GET', 'POST'] )
def editApp(): 
    posts = db.posts

    grantId = None
    appId = None
    field = ""
    edit = None

    form = EditAppForm()
    form1 = DeleteAppForm()
    if form.validate_on_submit():

        grantId = form.grantId.data
        appId = form.appId.data
        field = form.field.data
        edit = form.edit.data
        
        if field != "None":
            posts.update({"_id":grantId,"Applicants.appId":appId},{"$set":{"Applicants.$."+field: edit}},upsert=False)
            return redirect(url_for('editApp'))
        else:
            if form1.validate_on_submit():

                grantId = form.grantId.data
                appId = form.appId.data
                posts.update({"_id":grantId},{"$pull":{"Applicants":{'appId':appId}}})
                return redirect(url_for('editApp'))
    return render_template('editApp.html', form=form, form1=form1)

#edit evaluations
@app.route('/EditEval', methods=['GET', 'POST']  )
def editEval(): 
    posts = db.posts

    grantId = None
    appId = None
    field = None
    edit = None
    evalNum = None 

    
    form = EditEvalForm()
    if form.validate_on_submit():

        grantId = form.grantId.data
        appId = form.appId.data
        field = form.field.data
        edit = form.edit.data
        evalNum = form.evalNum.data

        evalNum1 = evalNum
        evalNum2 = evalNum1 - 1
        evalNumStr = str(evalNum2)
       
        posts.update({"_id":grantId,"Applicants.appId":appId,"Applicants.Evaluators.evalNum":evalNum},{"$set":{"Applicants.$.Evaluators."+evalNumStr+"."+field: edit}})
        return redirect(url_for('editEval'))
    return render_template('index.html', form=form)

#edit grants
@app.route('/EditGrant', methods=['GET', 'POST']  )
def EditGrant(): 
    posts = db.posts

    grantId = None
    field = None
    edit = None 
    
    form = EditGrantForm()
    if form.validate_on_submit():

        grantId = form.grantId.data
        field = form.field.data
        edit = form.edit.data

       
        posts.update({"_id":grantId},{"$set":{field:edit}})
        return redirect(url_for('EditGrant'))
    return render_template('index.html', form=form)

# deletes all Applicants for a certain grant
@app.route('/DelAllApp', methods=['GET', 'POST'] )
def DelAllApp():
    posts = db.posts

    grantId = None
    
    form = DelAllAppForm()
    if form.validate_on_submit():
        
        grantId = form.grantId.data

        posts.update({"_id":grantId},{"$unset":{"Applicants":1}})
        return redirect(url_for('DelAllApp'))
    return render_template('index.html',form=form)

#Grants sorted by students
@app.route('/student' )
def student():
    posts = db.posts
    return render_template('student.html', posts = posts)

#Grants sorted by faculty
@app.route('/faculty' )
def faculty():
    posts = db.posts
    return render_template('faculty.html',posts = posts)

#all current Applicants in all grants
@app.route('/CurApplicants' )
def CurApplicants():
    posts = db.posts
    print "test"
    return render_template('curApp.html',posts = posts)

'''
#delets all documents
@app.route('/Delete' )
def delete(): 
    posts = db.posts
    countB = posts.find().count()
    posts.remove()
    countA = posts.find().count()
    numDeleted = countB - countA
    return render_template('delete.html',numDeleted=numDeleted)

#for testing

@app.route('/test', methods=['GET', 'POST'] )
def test():
    posts = db.posts
    
    return render_template('test.html')
'''

if __name__ == '__main__':
    app.run(debug=True)



