from flask import Flask, jsonify, make_response, request
from pymongo import MongoClient

app = Flask(__name__)
client = MongoClient('mongodb://rashed:rashed123@ds063180.mongolab.com:063180/rashedsdb')
db = client.get_default_database()
posts=db.posts
# Get each grant information

@app.route('/grant/<int:id>/', methods=['GET'])
@app.route('/grant/<int:id>' , methods=['GET'])
def get_grant(id):
    if id>10000:
         return jsonify({'Error': 'Not found'})
    else:
         return jsonify(db.posts.find_one({"_id": id }))




# Insert data through post methoods 
#insert grant
@app.route('/post/grant', methods=['POST']) 
def insert_grant():
     
    if not request.json or not 'id' in request.json:
        return ("Insert data in propert format")
    elif request.json['id'] < 10000:   # Store json data sent as post request.  
        id = request.json['id']
        type = request.json.get('type',"") 
        award  = request.json.get('award',"")     
        awardAmount = request.json.get('awardAmount', "")
        numAwards = request.json.get('numAwards', "") 
        url = request.json.get('url',"")  
    
        posts.insert({"_id":id, "Type":type, "award":award, "awardAmount":awardAmount, "numAwards":numAwards, "url":url})
    else: 
        return ("Only id < 9 is allowed")
    return jsonify()   
  
@app.route('/post/grant/<int:grantid>/applicant', methods=['POST']) #insert applicant
def insert_applicant(grantid): 
    appId = request.json['appid']
    name  = request.json.get('name',"") 
    school  = request.json.get('school',"")     
    major = request.json.get('major', "")
    preAwards  = request.json.get('preAwards', "") 
    citizenship = request.json.get('citizenship',"")  
    time  = request.json.get('time',"")
    research = request.json.get('research',"")
    advisor = request.json.get('advisor',"")
    budget  = request.json.get('budget',"")
    url = request.json.get('url',"")
 

    posts.update({"_id" :grantid },{"$push":{"Applicants" : {"appId" : appId ,"Name" : name,"School" : school,"Major" : major,"preAwards" :preAwards,
    "citizenship" : citizenship,"Time" : time,"Research" : research,"Advisor" : advisor, "budget" :budget,"URL" : "www.url.com"}}})
     
    return jsonify({'Status': "Success!"})     
@app.route('/post/grant/<int:grantid>/applicant/<int:appid>/eval', methods=['POST']) # insert evaluators
def insert_evaluator(grantid,appid):
    #Hold the data passed to as json
    evalNum  = request.json['evalNum']
    abstractScore  = request.json.get('abstractScore',"") 
    goalsObjectivesScore  = request.json.get('goalsObjectiveScore',"")
    #insert/update the Evaluators field 
    print ( posts.find_one({'_id':grantid}) )
    posts.update({"_id":grantid,"Applicants.appId":appid},{"$push":
    {"Applicants.$.Evaluators":{"evalNum":evalNum, "abstractScore":abstractScore, "goalsObjectivesScore" : goalsObjectivesScore}}})
   
    return jsonify({'Status': "Success!"})




# Error 404 Handler

@app.errorhandler(404) 
def not_found(error):
    return make_response(jsonify({'Error': 'Not found'}), 404) 

# Call main function to start aplication. 
if __name__=='__main__':
    app.run(host='0.0.0.0', debug=True)


