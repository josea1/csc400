from flask import Flask, jsonify, make_response, request, render_template
from flask.ext.cors import CORS, cross_origin
from pymongo import MongoClient
from pymongo.errors import PyMongoError
import datetime
from datetime import date
from pytz import timezone
import pytz

fmt = '%Y-%m-%d %H:%M:%S %Z%z'
eastern = timezone('US/Eastern')
x=eastern.localize(datetime.datetime.today())
time=x.strftime(fmt)


server = "ds049661.mongolab.com"
port = 49661
db_name = "grantsystem"
username = "csc400"
password = "csc400"

conn = MongoClient(server, port)
db = conn[db_name]
db.authenticate(username, password)

app = Flask(__name__)
app.config['CORS_HEADERS'] = 'Content-Type'

cors = CORS(app)

@app.route('/')
def home():
	return render_template('home.html')

# Get each grant information
@app.route('/grant/<int:id>/', methods=['GET'])
@app.route('/grant/<int:id>' , methods=['GET'])
def get_grant(id):
    if id>10000:
         return jsonify({'Error': 'Not found'})
    else:
         return jsonify(db.posts.find_one({"_id": id }))



# Insert data through post methods 
@app.route('/post/grant', methods=['POST']) #insert grants
@cross_origin()
def insert_grant_student():
    posts=db.posts
   

    ID = request.json['GrantID']
    GrantType = request.json.get('GrantType',"") 
    GrantName  = request.json.get('GrantName',"")     
    GrantAmount = request.json.get('GrantAmount', "")
    NumberOfGrants = request.json.get('NumberOfGrants', "") 
    Student_url = request.json.get('url',"")
    Timestamp = request.json.get('Timestamp',"")
        
    try:
	    posts.insert({"_id":int(ID), "GrantType":GrantType, "GrantName":GrantName, "GrantAmount":GrantAmount, "NumberOfGrants":NumberOfGrants, "URL":Student_url ,"Timestamp": Timestamp})
    except PyMongoError as e:
        return (str(e))
    return ('Success')

@app.route('/delete/grant', methods=['POST']) #insert grants
@cross_origin()
def insert_grant_student_delete():
    posts=db.posts
   

    GrantName = request.json.get('GrantName',"") 
    Timestamp = request.json.get('Timestamp',"")
    print "--",GrantName,"--"
        
    try:
            posts.remove({"GrantName":GrantName})
    except:
            return ("Error")
    return ('Success')


# Insert data through put methods  
@app.route('/post/grant/applicant/student', methods=['PUT']) #insert applicants
@cross_origin()
def insert_applicant_student(): 
        posts = db.posts

        
        grantId = request.json['grantId']
        first = request.json.get("first", "")
        last = request.json.get("last","")
        mi = request.json.get("middleInitial","")
        school  = request.json.get('school',"")
        gradYear  = request.json.get('gradYear',"")
        prevAwards = request.json.get('prevAward',"")
        citizen = request.json.get('citizen',"")
        title = request.json.get('title',"")
        advisor = request.json.get('advisor',"")
        budget = request.json.get('budget',"")
        advisorEmail = request.json.get('advisorEmail',"")

        name = first+" "+mi+" "+last

        
        if posts.find_one({"Applicants.appId":('SPR_APP-'+str(100)) }, {'Applicants.$': 1}) == None:
            appId = ('SPR_APP-'+str(99))
        else:
            appId = posts.aggregate([
                {'$project':{'Applicants.appId':1}},
                {'$unwind':"$Applicants"},
                {'$sort':{"Applicants.appId":-1}},
                {'$limit':1}])['result'][0]['Applicants']['appId']
                              
        appID=int(appId[8:])
        if posts.update({"_id" :grantId },{"$push":{"Applicants" : {"appId" : appId[:8]+str(appID+1),"Name" : name,"School" : school,"gradYear" : gradYear,"prevAwards" :prevAwards,
                                                                "citizen" : citizen,"title" : title,"Advisor" : advisor,
                                                                "budget" :budget,"advisorEmail" : advisorEmail,"date": time}}})['nModified'] == 0:
            return ('Error: No Grant')
        else:
            return ('Success')

# Insert data through put methods  
@app.route('/post/grant/applicant/faculty', methods=['PUT']) #insert applicants
@cross_origin()
def insert_applicant_faculty(): 
        posts = db.posts
        
        grantId = request.json['grantId']
        first = request.json.get("first", "")
        last = request.json.get("last","")
        mi = request.json.get("middleInitial","")
        tenure = request.json.get("tenure","")
        #dept = request.json.get("dept","")
        institution = request.json.get("institution","")
        mailaddress = request.json.get("mailingAddress","")
        rank = request.json.get("rank","")
        phone = request.json.get("phone","")
        fax = request.json.get("fax","")
        email = request.json.get("email","")
        title = request.json.get("title","")
        daterange = request.json.get("dateRange","")
        courseNum = request.json.get("courseNum","")
        collab = request.json.get("collab","")
        budget = request.json.get("budget","")
        match = request.json.get("match","")
        prevAward = request.json.get("prevAward","")

        fullName = first+" "+mi+" "+last
        
        if posts.find_one({"Applicants.appId":('SPR_APP-'+str(100)) }, {'Applicants.$': 1}) == None:
            appId = ('SPR_APP-'+str(99))
        else:
            appId = posts.aggregate([
                {'$project':{'Applicants.appId':1}},
                {'$unwind':"$Applicants"},
                {'$sort':{"Applicants.appId":-1}},
                {'$limit':1}])['result'][0]['Applicants']['appId']
                              
        appID=int(appId[8:])
        if posts.update({"_id" :grantId },{"$push":{"Applicants" : {"appId" : appId[:8]+str(appID+1),"FullName" : fullName,"Tenure" : tenure,"School" : institution,"Mailaddress" :mailaddress,
                                                                "Rank" : rank,"Phone" : phone,"Fax" : fax,"email" : email,"Title" :title,"Daterange" : daterange,"Budget" :budget,"Match" :match,
                                                                "PrevAward" :prevAward, "CourseNum" :courseNum,"Collab" : collab,"date": time}}})['nModified'] == 0:
            return ('Error: No Grant')
        else:
            return ('Success')        

    
# Insert data through put methods 
@app.route('/post/grant/applicant/evaluator/faculty', methods=['PUT']) #insert evaluations
@cross_origin()
def insert_evaluator_faculty():
        posts = db.posts
       
        appId = request.json.get('facID')
        evalnum = request.json.get('evalnum')
        abstract = request.json.get('abstract',"")
        goals = request.json.get('goals',"")
        relevance = request.json.get('relevance',"")
        methods = request.json.get('methods',"")
        timeline = request.json.get('timeline',"")
        budget = request.json.get('budget',"")
        student = request.json.get('student',"")
        recent = request.json.get('recent',"")
        outcome = request.json.get('outcome',"")
        collab = request.json.get('collab',"")
        qualif = request.json.get('qualif',"")
        contact = request.json.get('contact',"")
        cover = request.json.get('cover',"")
        tenure = request.json.get('tenure',"")
        

        for post in posts.find({"Applicants.appId" : appId}, { "Applicants.$.Evaluators" : 1 }):
                for post2 in posts.find({"Applicants.appId" : appId}, { "Applicants.Evaluators" : 1 }):
                        if post2['Applicants'][0] != {}:
                                for i in range(len(post["Applicants"][0]["Evaluators"])):
                                        if evalnum == post["Applicants"][0]["Evaluators"][i]["evalnum"]:
                                                return ('Error: Duplicate Evaluator ID')

                                else:
                                        if posts.update({"Applicants.appId":appId},{"$push":{"Applicants.$.Evaluators":{"evalnum":evalnum,"abstract":abstract,"goals":goals,"relevance":relevance,
                                                         "methods":methods,"timeline":timeline,"budget":budget,"student":student,"recent":recent,"outcome":outcome,"collab":collab,
                                                         "qualif":qualif,"contact":contact,"tenure":tenure,"cover":cover,"date": time}}})['nModified'] == 0:
                                                return ('Error: No Applicant')
                                        else:
                                                return ('Success')

                        else:
                                if posts.update({"Applicants.appId":appId},{"$push":{"Applicants.$.Evaluators":{"evalnum":evalnum,"abstract":abstract,"goals":goals,"relevance":relevance,
                                                 "methods":methods,"timeline":timeline,"budget":budget,"student":student,"recent":recent,"outcome":outcome,"collab":collab,
                                                 "qualif":qualif,"contact":contact,"tenure":tenure,"cover":cover,"date": time}}})['nModified'] == 0:
                                        return ('Error: No Applicant')
                                else:
                                        return ('Success')

@app.route('/post/grant/applicant/evaluator/student', methods=['PUT']) #insert evaluations
@cross_origin()
def insert_evaluator_student():
        posts = db.posts
       
        appId = request.json.get('stuID')
        evalnum = request.json.get('evalNum')
        abstract = request.json.get('abstract',"")
        relevance = request.json.get('relation',"")
        budget = request.json.get('budget',"")
        method = request.json.get('method',"")
        feasible = request.json.get('feasible',"")
        outcome = request.json.get('outcome',"")
        career = request.json.get('career',"")
        contact = request.json.get('contact',"")
        cover = request.json.get('cover',"")


        for post in posts.find({"Applicants.appId" : appId}, { "Applicants.$.Evaluators" : 1 }):
                for post2 in posts.find({"Applicants.appId" : appId}, { "Applicants.Evaluators" : 1 }):
                        if post2['Applicants'][0] != {}:
                                for i in range(len(post["Applicants"][0]["Evaluators"])):
                                        if evalnum == post["Applicants"][0]["Evaluators"][i]["evalnum"]:
                                                return ('Error: Duplicate Evaluator ID')

                                else:
                                        if posts.update({"Applicants.appId":appId},{"$push":{"Applicants.$.Evaluators":{"evalnum":evalnum,"abstract":abstract,"feasible":feasible,
                                                                                    "relevance":relevance,"method":method,"budget":budget,"outcome":outcome,"career":career,
                                                                                    "contact":contact,"cover":cover,"date": time}}})['nModified'] == 0:
                                                return ('Error: No Applicant')
                                        else:
                                                return ('Success')

                        else:
                                if posts.update({"Applicants.appId":appId},{"$push":{"Applicants.$.Evaluators":{"evalnum":evalnum,"abstract":abstract,"feasible":feasible,
                                                                            "relevance":relevance,"method":method,"budget":budget,"outcome":outcome,"career":career,
                                                                            "contact":contact,"cover":cover,"date": time}}})['nModified'] == 0:
                                        return ('Error: No Applicant')
                                else:
                                        return ('Success')

 
# Error 404 Handler
@app.errorhandler(404) 
def not_found(error):
    return make_response(jsonify({'Error': 'Not found'}), 404) 

# Call main function to start aplication. 
if __name__=='__main__':
    app.run(host='0.0.0.0', debug=True, port=5001)

