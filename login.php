<!DOCTYPE html>
<html lang="en">
<head>
  <title>CT Grants | Login</title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
  <link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
  <link rel="stylesheet" href="css/layout.css" type="text/css" media="screen">
  <script type="text/javascript" src="js/jquery-1.6.min.js"></script>
  <script src="js/cufon-yui.js" type="text/javascript"></script>
  <script src="js/cufon-replace.js" type="text/javascript"></script>
  <script src="js/Open_Sans_400.font.js" type="text/javascript"></script>
  <script src="js/Open_Sans_Light_300.font.js" type="text/javascript"></script>
  <script src="js/Open_Sans_Semibold_600.font.js" type="text/javascript"></script>
  <script src="js/FF-cash.js" type="text/javascript"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-61427249-1', 'auto');
    ga('send', 'pageview');

  </script>  
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>
<body id="page3">
  <!-- header -->
  <div class="bg">
    <div class="main">
      <header>
        <div class="row-1">
          <a href="index.html"><img src="images/grantlogo.png" class="logo" style="display: inline-block;">
            <div id="titlepage" style="display: inline-block; font-size: 75px; color: white; margin:auto;padding: 50px;">
              Connecticut Space <br>
              Grant Consortium
            </div>
          </a>
        </div>
        <div class="row-2">
          <nav>
            <ul class="menu">
              <li><a href="index.html">Home Page</a></li>
              <li><a href="about.html">About Us</a></li>
              <li><a href="contacts.html">Contact Us</a></li>
              <li><a class="active" href="login.php">Login</a></li>
            </ul>
          </nav>
        </div>
      </header>
      <section id="content">
        <div class="padding">
          <div class="indent">
            <h2>Login</h2>      
            <?php
            session_start();
            session_unset();
            $errMsg='';

            if (isset($_GET['err'])){
             if($_GET['err'] == '2'){
              $errMsg = "Please Sign In";
            }
          }
          ?>
          <div style="text-align: center;">
            <div style="box-sizing: border-box; display: inline-block; width: auto; max-width: 480px; background-color: #FFFFFF; border: 2px solid #0361A8; border-radius: 5px; box-shadow: 0px 0px 8px #0361A8; margin: 50px auto auto;">
              <div style="background: #087ecf; border-radius: 5px 5px 0px 0px; padding: 15px;"><span style="font-family: calibri; color: #FFFFFF; font-size: 1.00em; font-weight:bold;">Enter your username and password</span></div>
              <div style="background: ; padding: 15px">
                <style type="text/css" scoped>
                  td { text-align:left; font-family: calibri; color: #064073; font-size: 1.00em; }
                  input { border: 1px solid #CCCCCC; border-radius: 5px; color: #666666; display: inline-block; font-size: 1.00em;  padding: 5px; width: 100%; }
                  input[type="button"], input[type="reset"], input[type="submit"] { font-family: calibri; height: auto; width: auto; cursor: pointer; box-shadow: 0px 0px 5px #0361A8; float: right; text-align:right; margin-top: 10px; margin-left:7px;}
                  table.center { margin-left:auto; margin-right:auto; }
                  .error { font-family: calibri; color: #D41313; font-size: 1.00em; }
                </style>
                <form method="post" action="login_action.php" name="aform" target="_top">
                  <input type="hidden" name="action" value="login">
                  <input type="hidden" name="hide" value="">
                  <table class='center'>
                    <span style="color:red;"><?=$errMsg?></span><br />
                    <tr><td>Username:</td><td><input type="text" name="login"></td></tr>
                    <tr><td>Password:</td><td><input type="password" name="password"></td></tr>
                    <tr><td>&nbsp;</td><td><input type="submit" value="Submit"></td></tr>
                    <tr><td colspan=2>&nbsp;</td></tr>
                  </table>
                </form>
              </div>
            </div>
          </div>​
          <br><br>
          
        </div>
      </div>
      
    </section>
    <footer>
     <div class="row-bot">
     </div>
   </footer>
 </div>
</div>
<script type="text/javascript">Cufon.now();</script>
</body>
</html>