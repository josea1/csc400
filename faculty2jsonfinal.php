<?php
	error_reporting(E_ALL);
	ini_set('display_errors',1);
	
				
		date_default_timezone_set('America/New_York');

			//input check
			for($i=0; $i<=4;$i++){
				if(empty($_POST['name'.$i])){
					echo "<font size=8>Please enter all information, a Name is missing</font><br>";
					echo "<a href=adminFaculty.html>Go Back</a><br>";
					die();
				}
		
				if(empty($_POST['amount'.$i])){
					echo "Please enter all information, an Award Amount is missing<br>";
					echo "<a href=adminFaculty.html>Go Back</a><br>";
					die();
				}
				else{
					$_POST['amount'.$i] = str_replace(".00", "", $_POST['amount'.$i]);
					$_POST['amount'.$i] = str_replace(",", "", $_POST['amount'.$i]);
					$_POST['amount'.$i] = str_replace("$", "", $_POST['amount'.$i]);
					if (is_numeric($_POST['amount'.$i]) && is_int(0+$_POST['amount'.$i])){
						// echo "all good<br>";
					}
					else{
						echo "The Award Amount fields must be a dollar amount<br>";
						echo "<a href=adminFaculty.html>Go Back</a><br>";
						die();
					}
				}
			
				if(empty($_POST['number'.$i])){
						echo "Please enter all information, a Number of Grants is missing<br>";
						echo "<a href=adminFaculty.html>Go Back</a><br>";
						die();
				}
				else{
					if (is_numeric($_POST['number'.$i]) && is_int(0+$_POST['number'.$i])){
						// echo "all good3<br>";
					}
					else{
						echo "The Number of Grant fields must be an integer<br>";
						echo "<a href=adminFaculty.html>Go Back</a><br>";
						die();
					}
				}
			}

			for($i=0; $i<=4;$i++){
				
					$grant = array( 
													"GrantID" 				  =>	  "$i",
													"GrantType"         =>    "Faculty",
													"GrantName"         =>    $_POST['name'.$i],
													"GrantAmount" 	    =>    $_POST['amount'.$i],
													"NumberOfGrants"    =>    $_POST['number'.$i],
													"url"   			      =>    "http://ctspacegrant.org/wp-content/uploads/2011/08/CSGC-faculty-booklet-spring-2015.pdf",
													"Timestamp"     	  =>    date('m-d-Y H:i:s'),
												);

					$json = json_encode($grant, JSON_PRETTY_PRINT);

					printf("<pre><font size=8>%s</font></pre>", $json);				

					// php5-curl
					$url = "http://ctspace.me/post/grant";						
					$ch = curl_init($url);                                                                      
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
					curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                  
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
					// curl_setopt($ch, CURLOPT_PORT, 5001);
					curl_setopt($ch, CURLOPT_TIMEOUT, 100);      
					curl_setopt($ch, CURLOPT_HEADER, 1); 
					curl_setopt($ch, CURLINFO_HEADER_OUT, true);                                                              
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
					    'Content-Type: application/json',                                                                                
					    'Content-Length: ' . strlen($json))                                                                       
					);                                                                                                                   
				
					if(curl_exec($ch) === false){
					    echo '<font size=15>Error: ' . curl_error($ch) . '</font><br>';
					}
					else{												      		
					    echo '<font size=15>JSON successfully sent</font><br>'; 

					}														  		
					$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					var_dump(curl_getinfo($ch,CURLINFO_HEADER_OUT));		
    				echo '<br>';
    				echo 'http code: ' . $httpcode .'<br>';
			}

			if(curl_exec($ch) === false){
			   	header('Location:adminBadSubmit.html');	 
			}
			else{												      		
			    header('Location:adminSubmit.html');			
			}
			curl_close($ch);

		
		// header('Location:adminFaculty.html')	
	
	
?>