<?php
session_start();

if($_SESSION['loggedin']){

	?>
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>CT Grants | Login</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
		<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen">
		<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
		<script src="js/cufon-yui.js" type="text/javascript"></script>
		<script src="js/cufon-replace.js" type="text/javascript"></script>
		<script src="js/Open_Sans_400.font.js" type="text/javascript"></script>
		<script src="js/Open_Sans_Light_300.font.js" type="text/javascript"></script>
		<script src="js/Open_Sans_Semibold_600.font.js" type="text/javascript"></script>
		<script src="js/FF-cash.js" type="text/javascript"></script>
		<script src="js/adminScript.js"></script>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-61427249-1', 'auto');
			ga('send', 'pageview');

		</script>
		<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>				
		<script>
			$(function() {
				$("#institution")
				.addClass("overflow");
			});
		</script>
		<style type="text/css">
			form {
				text-align: center;
				color: black;
			}
			
			#t1,
			#t3 {
				width: 700px;
				margin: auto;
				border-collapse: collapse;
				background: white;
			}
			
			#t2 {
				width: 100%;
				background: white;
			}
			
			#t1 td {
				border: 1px solid;
			}
			
			#t2 td {
				border: none;
				text-align: left;
			}
			
			#t1 th,
			#t3 th {
				text-align: center;
				background: white;
				height: 30px;
				border: 1px solid;
			}
			
			#t3 td {
				padding: 0px;
			}
			
			.tinner {
				margin: auto;
				height: 100%;
				width: 100%;
				border-collapse: collapse;
			}
			
			.tinner td {
				border-left: 1px solid;
				border-right: 1px solid;
				border-bottom: 1px solid;
			}
			
			.mylbl {
				height: 28px;
				background: white;
				text-align: left;
				text-indent: 10px;
			}
			
			.mylbl2 {
				background: white;
				text-align: left;
				text-indent: 10px;
			}
			
			.text {
				height: 100%;
				width: 97%;
				border: 0px;
			}
			
			.url {
				height: 25px;
				width: 99%;
				background: white;
				text-align: left;
				text-indent: 10px;
				border: 2px solid #dadada;
				border-radius: 7px;
			}
			
			.textarea {
				height: 94%;
				width: 99%;
				border: 0px;
			}
			
			.textmi {
				height: 100%;
				border: 0px;
			}
			
			#tdfirst1,
			#tdlast1 {
				width: 14%;
			}
			
			#tdfirst2 {
				width: 26%;
			}
			
			#tdlast2 {
				width: 34%;
			}
			
			#tdmi1 {
				width: 6%;
			}
			
			#tdrank1 {
				width: 14%;
			}
			
			#tdrank2 {
				width: 36%;
			}
			
			#tdtenure1 {
				width: 18%;
			}
			
			#tdconsortium1 {
				width: 35%;
			}
			
			#tddept1 {
				width: 16%;
			}
			
			#tdmailaddr1 {
				width: 17%;
			}
			
			#tdmailaddr2 {
				width: 36%;
			}
			
			#tdtown1 {
				width: 17%;
			}
			
			#tdtown2 {
				width: 30%;
			}
			
			#tdzip1 {
				width: 12%;
			}
			
			#tdzip2 {
				width: 37%;
			}
			
			#tdstate1 {
				width: 12%;
			}
			
			#tdstate2 {
				width: 37%;
			}
			
			#tdphone1 {
				width: 12%;
			}
			
			#tdphone2 {
				width: 38%;
			}
			
			#tdfax1 {
				width: 7%;
			}
			
			#tdemail1 {
				width: 12%;
			}
			
			#tdproject1 {
				height: 45px;
				width: 18%;
			}
			
			#tdperiod1 {
				width: 30%;
			}
			
			#tdcourse1 {
				width: 52%;
			}
			
			#tdcollabo1 {
				height: 75px;
				width: 30%;
			}
			
			#tdbudget1 {
				width: 20%;
			}
			
			#tdbudget2 {
				width: 20%;
				position: relative;
				overflow: hidden;
			}
			
			#tdmatch1 {
				width: 40%;
			}
			
			#tdaward1 {
				height: 40px;
				width: 32%;
			}
			
			#tdurl1 {
				width: 20%;
			}
			
			#tdurl2 {
				width: 80%;
			}
			
			.tinner p {
				margin: 0px;
				font-size: 8pt;
			}
			
			#tdsubmit {
				text-align: left;
			}
			
			.parsley-error {
				background-color: #f8dfdf;
			}
			
			.currency {
				position: absolute;
				top: 6px;
				left: 10px;
			}
			
			#budget {
				padding-left: 20px;
			}
			
			fieldset {
				border: 0;
			}
			
			select {
				width: 300px;
			}
			
			.overflow {
				height: 200px;
			}
		</style>
		<link type="text/css" rel="stylesheet" href="js/jquery-ui.min.css"></link>
		<link type="text/css" rel="stylesheet" href="js/jquery-ui.theme.min.css"></link>
		<link type="text/css" rel="stylesheet" href="js/jquery-ui.structure.min.css"></link>		
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>
<body id="page3">
	<!-- header -->
	<div class="bg">
		<div class="main">
			<header>
				<div class="row-1">
					<a href="index.html"><img src="images/grantlogo.png" class="logo" style="display: inline-block;">
						<div id="titlepage" style="display: inline-block; font-size: 75px; color: white; margin:auto;padding: 50px;">
							Connecticut Space <br>
							Grant Consortium
						</div>
					</a>
				</div>
				<div class="row-2">
					<nav>
						<ul class="menu">
							<li><a href="index.html">Home Page</a></li>
							<li><a href="about.html">About Us</a></li>
							<li><a href="contacts.html">Contact Us</a></li>
							<li><a class="active" href="login.php">Login</a></li>
						</ul>
					</nav>
				</div>
			</header>
			<section id="content">

				<form id="form" action="database.mongodb" method="post" data-parsley-focus="first">
					<p>
						<h2>Connecticut Space Grant College Consortium
							Faculty Application Cover Sheet 2015-2016 Program Year
						</h2>
					</p>

					<table id="t1">
						<tr>
							<th colspan="2">
								Award Type
							</th>
						</tr>
						<tr>
							<td>
								<table id="t2">
									<tr>
										<td>
											<input type="radio" name="type" class="grant" id="type1" value="3" required />
											<label for="type1">Curriculum Development</label>
										</td>
										<td>
											<input type="radio" name="type" class="grant" id="type2" value="2" />
											<label for="type2">Research Collaboration</label>
										</td>
									</tr>
									<tr>
										<td>
											<input type="radio" name="type" class="grant" id="type3" value="0" />
											<label for="type3">Research Grant</label>
										</td>
										<td>
											<input type="radio" name="type" class="grant" id="type4" value="1" />
											<label for="type4">Seed Research Grant</label>
										</td>
									</tr>
									<tr>
										<td>
											<input type="radio" name="type" class="grant" id="type5" value="4" />
											<label for="type5">STEM Education Research Grant</label>
										</td>
										<td>
										</td>
									</tr>

								</table>
							</td>
						</tr>

					</table>


					<br />


					<table id="t3">
						<tr>
							<th>
								Primary Investigator (PI)
							</th>
						</tr>
						<tr>
							<td>
								<table class="tinner">
									<tr>
										<td id="tdfirst1" class="mylbl">First Name</td>
										<td id="tdfirst2">
											<input id="first" type="text" name="first" class="text" data-parsley-trigger="blur" required autosize/>
										</td>
										<td id="tdlast1" class="mylbl">Last Name</td>
										<td id="tdlast2">
											<input id="last" type="text" name="last" class="text" data-parsley-trigger="blur" required autosize/>
										</td>
										<td id="tdmi1" class="mylbl">MI</td>
										<td id="tdmi2">
											<input id="mi" type="text" name="mi" class="textmi" data-parsley-maxlength="1" size="1" />
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<tr>
							<td>
								<table class="tinner">
									<tr>
										<td id="tdrank1" class="mylbl">Rank/Title</td>
										<td id="tdrank2">
											<input id="rank" type="text" name="rank" class="text" data-parsley-trigger="blur" required autosize/>
										</td>
										<td id="tdtenure1" class="mylbl">Tenure Status</td>
										<td id="tdtenure2">
											<input id="tenure" type="text" name="tenure" class="text" data-parsley-trigger="blur" required autosize/>
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<tr>
							<td>
								<table class="tinner">
									<tr>
										<td id="tdconsortium1" class="mylbl">Consortium Member Institution</td>
										<td id="tdconsortium2">
											<select name="institution" id="institution">
												<option>Albertus Magnus College</option>
												<option>Asnuntuck Community College</option>
												<option>Capital Community College</option>
												<option>Central Connecticut State University</option>
												<option>Charter Oak State College</option>
												<option>Connecticut College</option>
												<option>Eastern Connecticut State University</option>
												<option>Fairfield University</option>
												<option>Gateway Community College</option>
												<option>Goodwin College</option>
												<option>Hartford Seminary</option>
												<option>Holy Apostles College & Seminary</option>
												<option>Housatonic Community College</option>
												<option>LymeAcademy College of Fine Arts</option>
												<option>Manchester Community College</option>
												<option>Middlesex Community College</option>
												<option>Mitchell College</option>
												<option>Naugatuck Valley Community College</option>
												<option>Northwestern Connecticut Community College</option>
												<option>Norwalk Community College</option>
												<option>Paier College of Art </option>
												<option>Post University</option>
												<option>Quinebaug Valley Community College</option>
												<option>Quinnipiac University</option>
												<option>Rensselaer at Hartford</option>
												<option>Sacred Heart University</option>
												<option>Sanford-Brown College</option>
												<option selected="selected">Southern Connecticut State University</option>
												<option>Three Rivers Community College</option>
												<option>Trinity College</option>
												<option>Tunxis Community College</option>
												<option>United States Coast Guard Academy</option>
												<option>University of Bridgeport</option>
												<option>University of Connecticut</option>
												<option>University of Hartford</option>
												<option>University of New Haven</option>
												<option>University of Saint Joseph</option>
												<option>Wesleyan University</option>
												<option>Western Connecticut State University</option>
												<option>Yale University</option>
											</select>
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<tr>
							<td>
								<table class="tinner">
									<tr>
										<td id="tddept1" class="mylbl">Department</td>
										<td id="tddept2">
											<input id="dept" type="text" name="dept" class="text" data-parsley-trigger="blur" required autosize/>
										</td>
									</tr>
								</table>
							</td>
						</tr>


						<tr>
							<td>
								<table class="tinner">
									<tr>
										<td id="tdmailaddr1" class="mylbl">Street Address</td>
										<td id="tdmailaddr2">
											<input id="streetAddress" type="text" class="text" name="mailaddr" class="text" data-parsley-trigger="blur" required>
											<td id="tdtown1" class="mylbl">Town/City</td>
											<td id="tdtown2">
												<input id="city" type="text" class="text" name="mailaddr" class="text" data-parsley-trigger="blur" required>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table class="tinner">
											<tr>
												<td id="tdzip1" class="mylbl">Zip Code</td>
												<td id="tdzip2">
													<input id="zipCode" type="text" class="text" name="mailaddr" class="text" data-parsley-trigger="blur" required>
													<td id="tdstate1" class="mylbl">State</td>
													<td id="tdstate2">
														<input id="state" type="text" class="text" name="mailaddr" class="text" data-parsley-trigger="blur" required>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td>
												<table class="tinner">
													<tr>
														<td id="tdphone1" class="mylbl">Phone</td>
														<td id="tdphone2">
															<input id="phone" type="text" name="phone" class="text" placeholder="xxx-xxx-xxxx" data-parsley-trigger="blur" data-parsley-pattern="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$" required autosize/>
														</td>
														<td id="tdfax1" class="mylbl">Fax</td>
														<td id="tdfax2">
															<input id="fax" type="text" name="fax" class="text" placeholder="xxx-xxx-xxxx" data-parsley-trigger="blur" data-parsley-pattern="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$" autosize/>
														</td>
													</tr>
												</table>
											</td>
										</tr>

										<tr>
											<td>
												<table class="tinner">
													<tr>
														<td id="tdemail1" class="mylbl">Email</td>
														<td id="tdemail2">
															<input id="email" type="email" name="email" data-parsley-trigger="blur" class="text" required autosize/>
														</td>
													</tr>
												</table>
											</td>
										</tr>

										<tr>
											<td>
												<table class="tinner">
													<tr>
														<td id="tdproject1" class="mylbl2">Project Title
															<br />
															<br />
														</td>
														<td id="tdproject2">
															<textarea id="title" rows="2" cols="20" name="project" class="textarea" data-parsley-trigger="blur" required></textarea>
														</tr>
													</table>
												</td>
											</tr>

											<tr>
												<td>
													<table class="tinner">
														<tr>
															<td id="tdperiod1" class="mylbl">Proposed Performance Period</td>
															<td id="tdperiod2">
																<input id="dateTo" type="text" required>
																<span>to</span>
																<input id="dateFrom" type="text" required>
															</td>
														</tr>
													</table>
												</td>
											</tr>

											<tr>
												<td>
													<table class="tinner">
														<tr>
															<td id="tdcourse1" class="mylbl">Course Number (Curriculum Development Only)</td>
															<td id="tdcourse2">
																<input id="courseNum" type="text" name="course" class="textarea" data-parsley-trigger="blur" autosize disabled="disabled" />
															</td>
														</tr>
													</table>
												</td>
											</tr>

											<tr>
												<td>
													<table class="tinner">
														<tr>
															<td id="tdcollabo1" class="mylbl2">Collaboration (Specify
																<br />&nbsp Name & Contact Info)
																<br />
																<br />
															</td>
															<td id="tdcollabo2">
																<textarea id="collab" rows="3" cols="20" name="collabo" class="textarea" disabled="disabled"></textarea>
															</tr>
														</table>
													</td>
												</tr>


												<tr>
													<td>
														<table class="tinner">
															<tr>
																<td id="tdbudget1" class="mylbl">Budget Requested</td>
																<td id="tdbudget2">
																	<span class="currency">$</span>
																	<input id="budget" type="text" name="budget" class="text" data-parsley-trigger="blur" data-parsley-type="digits" required autosize/>
																</td>

																<td id="tdmatch1" class="mylbl">Match (1:1) Commitment Amount</td>
																<td id="tdmatch2">
																	<input id="match" type="text" name="match" data-parsley-type="digits" class="text" data-parsley-trigger="blur" required autosize/>
																</td>
															</tr>
														</table>
													</td>
												</tr>

												<tr>
													<td>
														<table class="tinner">
															<tr>
																<td id="tdaward1" class="mylbl2">Previous Space Grant Award(s)
																	<p>(Please list all: include title and date)</p>
																</td>
																<td id="tdaward2">
																	<textarea id="prevAward" rows="3" cols="20" name="award" class="textarea"></textarea>
																</td>
															</tr>
														</table>
													</td>
												</tr>

												<tr>
													<td>
														<table class="tinner">
															<td id="tdurl1" class="tdURL">Attachment URL:</td>
															<td id="tdurl2">
																<input id="url" type="text" name="url" class="url" placeholder="http://" data-parsley-type="url" required data-parsley-trigger="blur" />
															</td>
														</table>
													</td>
												</tr>

												<tr>
													<td>
														<table class="tinner">
															<td id="tdsubmit">
																<br />
																<input id="" type="submit" name="submit" value="Apply Now" class="btn btn-default validate" />
															</td>
														</table>
													</td>
												</tr>


											</table>
											<br />

										</form>
										<br />
									</section>
									<footer>
										<div class="row-bot">
										</div>
									</footer>
								</div>
							</div>

							<script src="//cdn-static.formisimo.com/tracking/js/tracking.js"></script>
							<script src="//cdn-static.formisimo.com/tracking/js/conversion.js"></script>
							
						</body>
						<script type='text/javascript' src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
						<script type='text/javascript' src="js/jquery-ui.min.js"></script>
						<script type='text/javascript' src="js/parsley.min.js"></script>

						<script type="text/javascript">
							function checkradio() {
								var course = document.getElementsByName('course')[0];
								var colla = document.getElementsByName('collabo')[0];
								if (document.getElementById('type1').checked) {
									course.disabled = false;
								} else {
									course.disabled = true;
									course.value = '';
								}
								if (document.getElementById('type2').checked) {
									colla.disabled = false;
								} else {
									colla.disabled = true;
									colla.value = '';
								}
							}
							$('.grant').click(function() {
								checkradio();
							});


    // Add form validation
    $('#form').parsley({
    	errorsWrapper: '',
    	errorTemplate: ''
    });

    $(document).ready(function() {

        // Add date picker to form
        $('#dateTo, #dateFrom').datepicker();

        $('form').submit(function(event) {

        	var grantId = $('.grant:checked').val();
        	var first = $("#first").val();
        	var last = $("#last").val();
        	var mi = $("#mi").val();
        	var rank = $("#rank").val();
        	var tenure = $("#tenure").val();
        	var Department = $("#dept").val();
        	var institution = $("#institution").val();
        	var streetAddress = $("#streetAddress").val();
        	var city = $("#city").val();
        	var zipCode = $("#zipCode").val();
        	var state = $("#state").val();
        	var phone = $("#phone").val();
        	phone = phone.replace(/-/g, '');
        	var fax = $("#fax").val();
        	fax = fax.replace(/-/g, '');
        	var email = $("#email").val();
        	var title = $("#title").val();
        	var dateTo = $("#dateTo").val();
        	var dateFrom = $("#dateFrom").val();
        	var courseNum = $("#courseNum").val();
        	var collab = $("#collab").val();
        	var budget = $("#budget").val();
        	var match = $("#match").val();
        	var prevAward = $("#prevAward").val();
        	var url = $("#url").val();


        	var formdata = JSON.stringify({
        		"grantId": grantId,
        		"first": first,
        		"last": last,
        		"middleInitial": mi,
        		"rank": rank,
        		"tenure": tenure,
        		"institution": institution,
        		"Department": Department,
        		"mailingAddress": streetAddress + " " + city + ", " + state + " " + zipCode,
        		"phone": phone,
        		"fax": fax,
        		"email": email,
        		"title": title,
        		"Daterange": dateTo + " - " + dateFrom,
        		"courseNum": courseNum,
        		"collab": collab,
        		"budget": budget,
        		"match": match,
        		"prevAward": prevAward,
        		"url": url
        	});
        	console.log(formdata);
        	response = $.ajax({
        		url: "http://ctspace.me/post/grant/applicant/faculty",
        		data: formdata,
        		type: "PUT",
        		dataType: 'json',
        		cache: false,
        		contentType: "application/json",
        		complete: function(response) {
        			console.log(response);
        			console.log(response.responseText);
        			alert(response.responseText);
        		}
        	});
        	event.preventDefault()
        });

});
</script>
<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o),
		m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', 'http://www.google-analytics.com/analytics.js', 'ga');
	ga('create', 'UA-61427249-1', 'auto');
	ga('send', 'pageview');
</script>

<!-- <script src="http://cdn-static.formisimo.com/tracking/js/conversion.js"></script> -->


<script type="text/javascript">Cufon.now();</script>
</html>

<?php
}else{
	header('Location: form.php?err=2');
}
?>