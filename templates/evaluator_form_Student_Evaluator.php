<?php
session_start();

if($_SESSION['loggedin']){

  ?>
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <title>CT Grants | Login</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="../css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="../css/layout.css" type="text/css" media="screen">
    <script type="text/javascript" src="../js/jquery-1.6.min.js"></script>
    <script src="../js/cufon-yui.js" type="text/javascript"></script>
    <script src="../js/cufon-replace.js" type="text/javascript"></script>
    <script src="../js/Open_Sans_400.font.js" type="text/javascript"></script>
    <script src="../js/Open_Sans_Light_300.font.js" type="text/javascript"></script>
    <script src="../js/Open_Sans_Semibold_600.font.js" type="text/javascript"></script>
    <script src="../js/FF-cash.js" type="text/javascript"></script>
    <script src="../js/adminScript.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-61427249-1', 'auto');
      ga('send', 'pageview');

    </script>   
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->  
<style type="text/css">
body {
  background-image: url(double-cluster-e1365181796716.jpg);
  clear: none;
  text-align: center;
  color: #000000;
}
.contentback form{
  border-radius: 75px;
  padding-left: 0px;
  margin: 50px;
}
.selector {
}
.linkcolors {
}
</style>

</head>
<body id="page3">
  <!-- header -->
  <div class="bg">
    <div class="main">
      <header>
        <div class="row-1">
          <a href="http://csc400.tk/index.html"><img src="../images/grantlogo.png" class="logo" style="display: inline-block;">
            <div id="titlepage" style="display: inline-block; font-size: 75px; color: white; margin:auto;padding: 50px;">
              Connecticut Space <br>
              Grant Consortium
            </div>
          </a>
        </div>
        <div class="row-2">
          <nav>
            <ul class="menu">
              <li><a href="http://csc400.tk/index.html">Home Page</a></li>
              <li><a href="http://csc400.tk/about.html">About Us</a></li>
              <li><a href="http://csc400.tk/contacts.html">Contact Us</a></li>
              <li><a class="active" href="http://csc400.tk/login.php">Login</a></li>
            </ul>
          </nav>
        </div>
      </header>
      <section id="content">

        <div id='grantMenu'>
          <center>
            <ul>
              <center>
                <li ><a href='student'><span>Student</span></a></li>
                <li ><a href='faculty'><span>Faculty</span></a></li>
              </center>
            </ul>
          </nav>
        </div>

<div class="contentback">
        <h1 style="color: #FFFFFF; margin: 0px; text-align: center;"> Student Evaluation (Beta)</h1>
<center>
        <form>
         <table width="80%" border="5" align="center" bgcolor="white">
          <tbody>
           <tr>
            <th colspan= "4"><p>Your Evaluator Number:	
              <input type="text" select id= 'evalNum' style="width: 125px;"></input>
            </p></th>
          </tr>
          <tr>
           <th colspan= "4"><p>Student Application Number: 
             <input type="text" select id= 'stuID' style="width: 100px;"></input>
           </p></th>
         </tr>
         <tr>
         </tr>
         <tr>
          <th style="color: black" scope="Criterion">Criterion: <span style="font-size: x-small">(mouse over for explanation)</span></th>
          <th style="color: black" scope="Max Points">Max Points:</th>
          <th style="color: black" scope="Points Awarded">Points Awarded:</th>
        </tr>
        <tr>
          <td style="text-align: left"><span title="How clear and testible is the specific research question?">Abstract</span></td>
          <td>10</td>
          <td>
           <select id= "abstract", style="width: 50px;">
             <option value="0">0</option>
             <option value="1">1</option>
             <option value="2">2</option>
             <option value="3">3</option>
             <option value="4">4</option>
             <option value="5">5</option>
             <option value="6">6</option>
             <option value="7">7</option>
             <option value="8">8</option>
             <option value="9">9</option>
             <option value="10">10</option>
           </select>
         </td>
       </tr>
       <tr>
        <td style="text-align: left">Relation to NASA's Strategic Goals</td>
        <td>15</td>
        <td>
         <select id= "relation", style="width: 50px;">
           <option value=" 0">0</option>
           <option value=" 1">1</option>
           <option value=" 2">2</option>
           <option value=" 3">3</option>
           <option value=" 4">4</option>
           <option value=" 5">5</option>
           <option value=" 6">6</option>
           <option value=" 7">7</option>
           <option value=" 8">8</option>
           <option value=" 9">9</option>
           <option value="10">10</option>
           <option value="11">11</option>
           <option value="12">12</option>
           <option value="13">13</option>
           <option value="14">14</option>
           <option value="15">15</option>
         </select>
       </td>
     </tr>
     <tr>
      <td style="text-align: left">Methodology</td>
      <td>15</td>
      <td>
      	<select id = "method", style="width: 50px;">
         <option value=" 0">0</option>
         <option value=" 1">1</option>
         <option value=" 2">2</option>
         <option value=" 3">3</option>
         <option value=" 4">4</option>
         <option value=" 5">5</option>
         <option value=" 6">6</option>
         <option value=" 7">7</option>
         <option value=" 8">8</option>
         <option value=" 9">9</option>
         <option value="10">10</option>
         <option value="11">11</option>
         <option value="12">12</option>
         <option value="13">13</option>
         <option value="14">14</option>
         <option value="15">15</option>
       </select>
     </td>
   </tr>
   <tr>
    <td style="text-align: left">Feasibility & Timeline (Planning)</td>
    <td>15</td>
    <td>
     <select id= "feasible", style="width: 50px;">
       <option value=" 0">0</option>
       <option value=" 1">1</option>
       <option value=" 2">2</option>
       <option value=" 3">3</option>
       <option value=" 4">4</option>
       <option value=" 5">5</option>
       <option value=" 6">6</option>
       <option value=" 7">7</option>
       <option value=" 8">8</option>
       <option value=" 9">9</option>
       <option value="10">10</option>
       <option value="11">11</option>
       <option value="12">12</option>
       <option value="13">13</option>
       <option value="14">14</option>
       <option value="15">15</option>
     </select>
   </td>
 </tr>
 <tr>
  <td style="text-align: left">Budget Narrative and Worksheet</td>
  <td>15</td>
  <td>
   <select id= "budget", style= "width: 50px;">
     <option value=" 0">0</option>
     <option value=" 1">1</option>
     <option value=" 2">2</option>
     <option value=" 3">3</option>
     <option value=" 4">4</option>
     <option value=" 5">5</option>
     <option value=" 6">6</option>
     <option value=" 7">7</option>
     <option value=" 8">8</option>
     <option value=" 9">9</option>
     <option value="10">10</option>
     <option value="11">11</option>
     <option value="12">12</option>
     <option value="13">13</option>
     <option value="14">14</option>
     <option value="15">15</option>
   </select>
 </td>
</tr>
<tr>
  <td style="text-align: left">Expected Outcome</td>
  <td>20</td>
  <td>
   <select id= "outcome", style="width: 50px;">
     <option value=" 0">0</option>
     <option value=" 1">1</option>
     <option value=" 2">2</option>
     <option value=" 3">3</option>
     <option value=" 4">4</option>
     <option value=" 5">5</option>
     <option value=" 6">6</option>
     <option value=" 7">7</option>
     <option value=" 8">8</option>
     <option value=" 9">9</option>
     <option value="10">10</option>
     <option value="11">11</option>
     <option value="12">12</option>
     <option value="13">13</option>
     <option value="14">14</option>
     <option value="15">15</option>
     <option value="16">16</option>
     <option value="17">17</option>
     <option value="18">18</option>
     <option value="19">19</option>
     <option value="20">20</option>
   </select>
 </td>
</tr>
<tr>
  <td style="text-align: left">Career Potential</td>
  <td>5</td>
  <td>
   <select id= "career", style="width: 50px;">
     <option value=" 0">0</option>
     <option value=" 1">1</option>
     <option value=" 2">2</option>
     <option value=" 3">3</option>
     <option value=" 4">4</option>
     <option value=" 5">5</option>
   </select>
 </td>
</tr>
<tr>
  <td style="text-align: left">Contact With NASA</td>
  <td>5</td>
  <td>
   <select id= "contact", style="width: 50px;">
     <option value=" 0">0</option>
     <option value=" 1">1</option>
     <option value=" 2">2</option>
     <option value=" 3">3</option>
     <option value=" 4">4</option>
     <option value=" 5">5</option>
   </select>
 </td>
</tr>
<tr>

  <th colspan= "4">Cover Letter URL: <input type="text" select id='cover' style="width: 300px;"></input></th>
</tr>
<tr>
  <br>
  <th colspan= "4"><br><input type="submit" value="Submit" /><br></th>
</tr>
</tbody>
</table>
</p>
<br>
</form>
</center>
</section>
<footer>
  <div class="row-bot">
  </div>
</footer>
</div>
</div>
<script type="text/javascript">Cufon.now();</script>


<script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
<script>
  $(document).ready(function() {
    $('form').on('submit',function() {
      
      var eva = parseInt($("#evalNum").val());
      var stu = $("#stuID").val();
      var ab = $("#abstract").val();
      var rel = $("#relation").val();
      var met = $("#method").val();
      var fea = $("#feasible").val();
      var bud = $("#budget").val();
      var out = $("#outcome").val();
      var car = $("#career").val();
      var con = $("#contact").val();
      var cov = $("#cover").val();
      
      var formdata = JSON.stringify({ "stuID" : stu,"evalnum" : eva, "abstract" : ab, "relation" : rel,"budget" : bud, "method" : met, "feasible" : fea,  
      "outcome" : out, "career" : car,"contact" : con, "cover" : cov});
      console.log(formdata);
      
      response = $.ajax( { 
        url: "http://ctspace.me/post/grant/applicant/evaluator/student",
        data: formdata,
        type: "PUT",
        crossDomain: "True",
        dataType: 'json',
        contentType: "application/json", 
        complete: function (response) {
          console.log(response.responseText);
          if (response.responseText == "Success") {
            alert('Submittion successful. Thank you for your submittion.');
          }
          else {
            alert('An Error has Occured');
          }          
        }
      })
      event.preventDefault()
    });
  }); 
</script> 
<script src="//cdn-static.formisimo.com/tracking/js/tracking.js"></script>
<script src="//cdn-static.formisimo.com/tracking/js/conversion.js"></script>
</body>
</html>

<?php
}else{
  header('Location: form.php?err=2');
}
?>