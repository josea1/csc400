<?php
session_start();

if($_SESSION['loggedin']){

  ?>
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <title>CT Grants | Login</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="../css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="../css/layout.css" type="text/css" media="screen">
    <script type="text/javascript" src="../js/jquery-1.6.min.js"></script>
    <script src="../js/cufon-yui.js" type="text/javascript"></script>
    <script src="../js/cufon-replace.js" type="text/javascript"></script>
    <script src="../js/Open_Sans_400.font.js" type="text/javascript"></script>
    <script src="../js/Open_Sans_Light_300.font.js" type="text/javascript"></script>
    <script src="../js/Open_Sans_Semibold_600.font.js" type="text/javascript"></script>
    <script src="../js/FF-cash.js" type="text/javascript"></script>
    <script src="../js/adminScript.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-61427249-1', 'auto');
      ga('send', 'pageview');

    </script>   
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->     
<style type="text/css">
body {
  background-image: url(double-cluster-e1365181796716.jpg);
  clear: none;
  text-align: center;
  color: #000000;
}
.contentback form{
  border-radius: 75px;
  padding-left: 0px;
  margin: 50px;
}
.selector {
}
.linkcolors {
}
</style>

</head>
<body id="page3">
  <!-- header -->
  <div class="bg">
    <div class="main">
      <header>
        <div class="row-1">
          <a href="http://csc400.tk/index.html"><img src="../images/grantlogo.png" class="logo" style="display: inline-block;">
            <div id="titlepage" style="display: inline-block; font-size: 75px; color: white; margin:auto;padding: 50px;">
              Connecticut Space <br>
              Grant Consortium
            </div>
          </a>
        </div>
        <div class="row-2">
          <nav>
            <ul class="menu">
              <li><a href="http://csc400.tk/index.html">Home Page</a></li>
              <li><a href="http://csc400.tk/about.html">About Us</a></li>
              <li><a href="http://csc400.tk/contacts.html">Contact Us</a></li>
              <li><a class="active" href="http://csc400.tk/login.php">Login</a></li>
            </ul>
          </nav>
        </div>
      </header>
      <section id="content">

        <div id='grantMenu'>
          <center>
            <ul>
              <center>
                <li ><a href='student'><span>Student</span></a></li>
                <li ><a href='faculty'><span>Faculty</span></a></li>
              </center>
            </ul>
          </nav>
        </div>

<div class="contentback">
<h1 style="color: #FFFFFF; margin: 0px; text-align: center;"> Faculty Evaluation (Beta)</h1>
<center>
<form>
  <table width="80%" border="5" align="center" bgcolor="white">
 		
    <tbody>
   	<tr>
		<th colspan= "4"><p>Your Evaluator Number:	
        <input type="text" select id= 'evalnum' style="width: 125px;"></input>
		</p></th>
    </tr>
    <tr>
   	  	<th colspan= "4"><p>Faculty Application Number: 
<select id='facID'>
{% for ID in l %}
<option value='{{ID}}'>{{ID}}</option>
{%endfor%}  
</select>
   	  	</p></th>
    </tr>
    <tr>
    </tr>
    <tr>
      <th width="53%" style="color: black" scope="Criterion">Criterion: <span style="font-size: x-small">(mouse over for explanation)</span></th>
      <th width="20%" style="text-align: center" scope="Max Points">Max Points:</th>
      <th width="27%" style="color: black" scope="Points Awarded">Points Awarded:</th>
    </tr>
    <tr>
      <td style="text-align: left"><span title="How clear, concise, and able to give the reader a sense of overall scope is the proposal?">Abstract</span></td>
      <td style="text-align: center">5</td>
      <td style="text-align: center">
      	<select id= 'abstract', style="width: 50px;">
			<option value="0">0</option>
   		 	<option value="1">1</option>
    		<option value="2">2</option>
    		<option value="3">3</option>
    		<option value="4">4</option>
            <option value="5">5</option>
		</select>
      </td>
    </tr>
    <tr>
      <td style="text-align: left"><span title="How well are the goals and objectives clearly stated? How strong are the reasons offered to pursue the project?">Goals and Objectives</span></td>
      <td style="text-align: center">10</td>
      <td style="text-align: center">
      	<select id = 'goals', style="width: 50px;">
			<option value="0">0</option>
   		 	<option value="1">1</option>
    		<option value="2">2</option>
    		<option value="3">3</option>
    		<option value="4">4</option>
            <option value="5">5</option>
   		 	<option value="6">6</option>
    		<option value="7">7</option>
    		<option value="8">8</option>
    		<option value="9">9</option>
            <option value="10">10</option>
		</select>
      </td>
    </tr>
    <tr>
      <td style="text-align: left"><span title="How well does the proposed project align with NASA's strategic goals?">Relevance to NASA's strategic goals</span></td>
      <td style="text-align: center">10</td>
      <td style="text-align: center">
      	<select id= 'relevance', style="width: 50px;">
			<option value="0">0</option>
   		 	<option value="1">1</option>
    		<option value="2">2</option>
    		<option value="3">3</option>
    		<option value="4">4</option>
            <option value="5">5</option>
   		 	<option value="6">6</option>
    		<option value="7">7</option>
    		<option value="8">8</option>
    		<option value="9">9</option>
            <option value="10">10</option>
		</select>
      </td>
    </tr>
    <tr>
      <td style="text-align: left"><span title="How well is the proposal detailed, explaining the methods and procedures needed to achieve the goals and objectives?">Methods and Procedures</span></td>
      <td style="text-align: center">15</td>
      <td style="text-align: center">
      	<select id = 'methods', style="width: 50px;">
			<option value=" 0">0</option>
   		 	<option value=" 1">1</option>
    		<option value=" 2">2</option>
    		<option value=" 3">3</option>
    		<option value=" 4">4</option>
            <option value=" 5">5</option>
   		 	<option value=" 6">6</option>
    		<option value=" 7">7</option>
    		<option value=" 8">8</option>
    		<option value=" 9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
    		<option value="12">12</option>
    		<option value="13">13</option>
    		<option value="14">14</option>
            <option value="15">15</option>
		</select>
      </td>
    </tr>
    <tr>
      <td style="text-align: left"><span title="How clear and detailed is the proposed timeline? How strong is the institutional support?">Timeline and Feasibility</span></td>
      <td style="text-align: center">10</td>
      <td style="text-align: center">
      	<select id= 'timeline', style="width: 50px;">
			<option value="0">0</option>
   		 	<option value="1">1</option>
    		<option value="2">2</option>
    		<option value="3">3</option>
    		<option value="4">4</option>
            <option value="5">5</option>
   		 	<option value="6">6</option>
    		<option value="7">7</option>
    		<option value="8">8</option>
    		<option value="9">9</option>
            <option value="10">10</option>
		</select>
      </td>
    </tr>
    <tr>
      <td style="text-align: left"><span title="How clear and detailed is the budget plan? How strong are the justifications for the expenditures, spanning the entire length of the program?">Budget Narrative and Worksheet</span></td>
      <td style="text-align: center">10</td>
      <td style="text-align: center">
      	<select id= 'budget', style="width: 50px;">
			<option value="0">0</option>
   		 	<option value="1">1</option>
    		<option value="2">2</option>
    		<option value="3">3</option>
    		<option value="4">4</option>
            <option value="5">5</option>
   		 	<option value="6">6</option>
    		<option value="7">7</option>
    		<option value="8">8</option>
    		<option value="9">9</option>
            <option value="10">10</option>
		</select>
      </td>
    </tr>
    <tr>
      <td style="text-align: left"><span title="How well do students play a role in the project, and how well are they included in the budget?">Student Involvement</span></td>
      <td style="text-align: center">5</td>
      <td style="text-align: center">
      	<select id= 'student', style="width: 50px;">
			<option value="0">0</option>
   		 	<option value="1">1</option>
    		<option value="2">2</option>
    		<option value="3">3</option>
    		<option value="4">4</option>
            <option value="5">5</option>
		</select>
      </td>
    </tr>
    <tr>
      <td style="text-align: left"><span title="If the applicant recieved an award last year assign '0'. 2 years ago, assign '1', 3 years assign '2' etc...  If the applicants have never recieved an award assign '5'.">Recent Award</span></td>
      <td style="text-align: center">5</td>
      <td style="text-align: center">
      	<select id= 'recent', style="width: 50px;">
			<option value="0">0</option>
   		 	<option value="1">1</option>
    		<option value="2">2</option>
    		<option value="3">3</option>
    		<option value="4">4</option>
            <option value="5">5</option>
		</select>
      </td>
    </tr>
    <tr>
      <td style="text-align: left"><span title="How great is the potential for innovation, publication, or future funding? How well-defined is the plan for disseminating findings?">Expected Outcome</span></td>
      <td style="text-align: center">10</td>
      <td style="text-align: center">
      	<select id='outcome', style="width: 50px;">
			<option value="0">0</option>
   		 	<option value="1">1</option>
    		<option value="2">2</option>
    		<option value="3">3</option>
    		<option value="4">4</option>
            <option value="5">5</option>
   		 	<option value="6">6</option>
    		<option value="7">7</option>
    		<option value="8">8</option>
    		<option value="9">9</option>
            <option value="10">10</option>
		</select>
      </td>
    </tr>
    <tr>
      <td style="text-align: left"><span title="How strong is the evidence of collaboration across disciplines, colleges/universities or with external partners?">Collaboration</span></td>
      <td style="text-align: center">5</td>
      <td style="text-align: center">
      	<select id='collab', style="width: 50px;">
			<option value="0">0</option>
   		 	<option value="1">1</option>
    		<option value="2">2</option>
    		<option value="3">3</option>
    		<option value="4">4</option>
            <option value="5">5</option>
		</select>
      </td>
    </tr>
    <tr>
      <td style="text-align: left"><span title="How strong is the evidence that the applicant(s) is/are capable of completing project based on prior work and future plans?">Faculty Qualifications</span></td>
      <td style="text-align: center">5</td>
      <td style="text-align: center">
      	<select id='qualif', style="width: 50px;">
			<option value="0">0</option>
   		 	<option value="1">1</option>
    		<option value="2">2</option>
    		<option value="3">3</option>
    		<option value="4">4</option>
            <option value="5">5</option>
		</select>
      </td>
    </tr>
    <tr>
      <td style="text-align: left"><span title="How strong is the evidence of NASA support? If the applicant has made no effort to contact NASA, assign '0'.">Contact with NASA</span></td>
      <td style="text-align: center">5</td>
      <td style="text-align: center">
      	<select id='contact', style="width: 50px;">
			<option value="0">0</option>
   		 	<option value="1">1</option>
    		<option value="2">2</option>
    		<option value="3">3</option>
    		<option value="4">4</option>
            <option value="5">5</option>
		</select>
      </td>
    </tr>
    <tr>
      <td style="text-align: left"><span title="If the applicant is full-time and pre-tenure, assign '5', otherwise, assign '0'.">Tenure Status</span></td>
      <td style="text-align: center">5</td>
      <td style="text-align: center">
      	<select id='tenure', style="width: 50px;">
			<option value="0">0</option>
            <option value="5">5</option>
		</select>
      </td>
    </tr>
    <tr>
   	  		<th colspan= "4">Cover Letter URL: <input type="text" select id='cover' style="width: 300px;"></input></th>
    </tr>
    <tr>
    <br>
    <th colspan= "4"><br><input type="submit" value="Submit" /><br></th>
    </tr>
  </tbody>
</table>
</p>
<br>
</form>
</section>
<footer>
  <div class="row-bot">
  </div>
</footer>
</div>
</div>
<script type="text/javascript">Cufon.now();</script>

<script src="http://code.jquery.com/jquery-2.1.3.min.js"></script>
<script>
  $(document).ready(function() {
    $('form').on('submit',function() {
      
      var eva = $("#evalnum").val();
      var fac = $("#facID").val();
      var ab = $("#abstract").val();
      var goa = $("#goals").val();
      var rel = $("#relevance").val();
      var met = $("#methods").val();
      var tim = $("#timeline").val();
      var bud = $("#budget").val();
      var stu = $("#student").val();
      var rec = $("#recent").val();
      var out = $("#outcome").val();
      var col = $("#collab").val();
      var qua = $("#qualif").val();
      var con = $("#contact").val();
      var ten = $("#tenure").val();
      var cov = $("#cover").val();
      
      var formdata = JSON.stringify({"evalnum" : eva, "facID" : fac, "abstract" : ab, "goals" : goa, "relevance" : rel, "methods" : met, 
        "timeline" : tim, "budget" : bud, "student" : stu, "recent" : rec, "outcome" : out, "collab" : col, 
          "qualif" : qua, "contact" : con, "tenure" : ten, "cover" : cov});
      console.log(formdata);
      
      response = $.ajax( { 
        url: "http://ctspace.me/post/grant/applicant/evaluator/faculty",
        data: formdata,
        type: "PUT",
        crossDomain: "True",
        dataType: 'json',
        contentType: "application/json", 
        complete: function (response) {
          console.log(response);
          if (response.responseText == "Success") {
            alert('Submittion successful. Thank you for your submittion.');
          }
          else {
            alert('An Error has Occured');
          }          
        }
      })    
      event.preventDefault()
    });
  });
</script>
<script src="//cdn-static.formisimo.com/tracking/js/tracking.js"></script>
<script src="//cdn-static.formisimo.com/tracking/js/conversion.js"></script>
</body>
</html>
<?php
}else{
  header('Location: form.php?err=2');
}
?>