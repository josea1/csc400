<?php
session_start();

if($_SESSION['loggedin']){

	?>
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>Student Application Form</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
		<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen">
		<script type="text/javascript" src="js/jquery-1.6.min.js"></script>
		<script src="js/cufon-yui.js" type="text/javascript"></script>
		<script src="js/cufon-replace.js" type="text/javascript"></script>
		<script src="js/Open_Sans_400.font.js" type="text/javascript"></script>
		<script src="js/Open_Sans_Light_300.font.js" type="text/javascript"></script>
		<script src="js/Open_Sans_Semibold_600.font.js" type="text/javascript"></script>
		<script src="js/FF-cash.js" type="text/javascript"></script>
		<script src="js/adminScript.js"></script>
		<link type="text/css" rel="stylesheet" href="js/jquery-ui.min.css"></link>
		<link type="text/css" rel="stylesheet" href="js/jquery-ui.theme.min.css"></link>
		<link type="text/css" rel="stylesheet" href="js/jquery-ui.structure.min.css"></link>
		<script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-61427249-1', 'auto');
			ga('send', 'pageview');

		</script>
		<script>
			$(function() {
				$('#gradYear').datepicker( {
					changeMonth: true,
					changeYear: true,
					showButtonPanel: true,
					dateFormat: 'MM yy',
					onClose: function(dateText, inst) { 
						var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
						var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
						$(this).datepicker('setDate', new Date(year, month, 1));
					}
				});
			});

			$(function() {
				$("#uniName")
				.addClass("overflow");
			});
		</script>

		<style type="text/css">
			form {
				text-align: center;
				color: black;
			}
			
			#t1,
			#t3 {
				width: 700px;
				margin: auto;
				border-collapse: collapse;
				background: white;
			}
			
			#t2 {
				width: 100%;
				background: white;
			}
			
			#t1 td {
				border: 1px solid;
			}
			
			#t2 td {
				border: none;
				text-align: left;
			}
			
			#t1 th,
			#t3 th {
				text-align: center;
				background: white;
				height: 30px;
				border: 1px solid;
			}
			
			#t3 td {
				padding: 0px;
			}
			
			.tinner {
				margin: auto;
				height: 100%;
				width: 100%;
				border-collapse: collapse;
			}
			
			.tinner td {
				border-left: 1px solid;
				border-right: 1px solid;
				border-bottom: 1px solid;
			}
			
			.mylbl {
				height: 28px;
				background: white;
				text-align: left;
				text-indent: 10px;
			}
			
			.mylbl2 {
				height: 25px;
				background: white;
				text-align: left;
				text-indent: 10px;
			}
			
			.url {
				height: 25px;
				width: 330%;
				background: white;
				text-align: left;
				text-indent: 10px;
				border: 2px solid #dadada;
				border-radius: 7px;
			}
			
			.text {
				height: 100%;
				width: 98%;
				border: 0px;
			}
			
			.textmi {
				height: 100%;
				border: 0px;
			}
			
			.textarea {
				height: 94%;
				width: 99%;
				border: 0px;
			}
			
			#tdfirst1,
			#tdlast1 {
				width: 14%;
			}
			
			#tdfirst2 {
				width: 26%;
			}
			
			#tdlast2 {
				width: 34%;
			}
			
			#tdmi1 {
				width: 6%;
			}
			
			#tdUni1 {
				width: 27%;
			}
			
			#tdUni2 {
				width: 73%;
			}
			
			#tdMajor1 {
				width: 10%;
			}
			
			#tdMajor2 {
				width: 90%;
			}
			
			#tdCitizen1 {
				width: 14%;
			}
			
			#tdCitizen2 {
				width: 20%;
			}
			
			#tdgradYear1 {
				width: 28%;
			}
			
			#tdemail1 {
				width: 12%;
			}
			
			#tdproject1 {
				height: 45px;
				width: 18%;
			}
			
			#tdperiod1 {
				width: 30%;
			}
			
			#tdcourse1 {
				width: 52%;
			}
			
			#tdadvisor1 {
				width: 17%;
			}
			
			#tdadvisor2 {
				width: 30%;
			}
			
			#tdemail1 {
				width: 15%;
			}
			
			#tdemail2 {
				width: 30%;
			}
			
			#tdbudget1 {
				width: 4%;
			}
			
			#tdbudget2 {
				width: 20%;
				position: relative;
				overflow: hidden;
			}
			
			#tdmatch1 {
				width: 40%;
			}
			
			#tdaward1 {
				height: 75px;
				width: 32%;
			}
			
			#tdurl1 {
				height: 45px;
				width: 25%;
			}
			
			#tdurl2 {
				height: 45px;
				width: 75%;
			}
			
			.tinner p {
				margin: 0px;
				font-size: 8pt;
			}
			
			#tdsubmit {
				text-align: left;
			}
			
			.parsley-error {
				background-color: #f8dfdf;
			}
			
			.currency {
				position: absolute;
				top: 6px;
				left: 10px;
			}
			
			#budget {
				padding-left: 20px;
			}
			
			fieldset {
				border: 0;
			}
			
			select {
				width: 300px;
			}
			
			.overflow {
				height: 200px;
			}
		</style>
		<link type="text/css" rel="stylesheet" href="js/jquery-ui.min.css"></link>
		<link type="text/css" rel="stylesheet" href="js/jquery-ui.theme.min.css"></link>
		<link type="text/css" rel="stylesheet" href="js/jquery-ui.structure.min.css"></link>			
<!--[if lt IE 9]>
<script type="text/javascript" src="js/html5.js"></script>
<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>
<body id="page3">
	<!-- header -->
	<div class="bg">
		<div class="main">
			<header>
				<div class="row-1">
					<a href="index.html"><img src="images/grantlogo.png" class="logo" style="display: inline-block;">
						<div id="titlepage" style="display: inline-block; font-size: 75px; color: white; margin:auto;padding: 50px;">
							Connecticut Space <br>
							Grant Consortium
						</div>
					</a>
				</div>
				<div class="row-2">
					<nav>
						<ul class="menu">
							<li><a href="index.html">Home Page</a></li>
							<li><a href="about.html">About Us</a></li>
							<li><a href="contacts.html">Contact Us</a></li>
							<li><a class="active" href="login.php">Login</a></li>
						</ul>
					</nav>
				</div>
			</header>
			<section id="content">
				<center>
					<h2>Connecticut Space Grant College Consortium Student Application Cover Sheet 2015-2016 Program Year</h2>
				</center>
				<form id="form" action="database.mongodb" method="post" data-parsley-focus="first">
					<table id="t1">
						<tr>
							<th colspan="2">Award Type</th>
						</tr>
						<tr>
							<td>
								<table id="t2">
									<tr>
										<td>
											<input type="radio" name="grant" class="grant" id="type1" value="5" required />
											<label for="type1">Graduate Research Fellowship</label>
										</td>
										<td>
											<input type="radio" name="grant" class="grant" id="type2" value="6" />
											<label for="type2">Undergraduate Research Fellowship</label>
										</td>
									</tr>
									<tr>
										<td>
											<input type="radio" name="grant" class="grant" id="type3" value="7" />
											<label for="type3">Undergraduate Directed Campus Scholarship</label>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<br />

					<table id="t3">
						<tr>
							<th>Primary Investigator (PI)</th>
						</tr>
						<tr>
							<td>
								<table class="tinner">
									<tr>
										<td id="tdfirst1" class="mylbl">First Name</td>
										<td id="tdfirst2">
											<input id="first" type="text" name="first" class="text" data-parsley-trigger="blur" required autosize/>
										</td>
										<td id="tdlast1" class="mylbl">Last Name</td>
										<td id="tdlast2">
											<input id="last" type="text" name="last" class="text" data-parsley-trigger="blur" required autosize/>
										</td>
										<td id="tdmi1" class="mylbl">MI</td>
										<td id="tdmi2">
											<input id="mi" type="text" name="mi" class="textmi" data-parsley-maxlength="1" size="1" />
										</td>
									</tr>
								</table>
							</td>
						</tr>

						<tr>
							<td>
								<table class="tinner">
									<tr>
										<td id="tdUni1" class="mylbl">University/College Name</td>
										<td id="tdUni2">
											<select name="uniName" id="uniName">
												<option>Albertus Magnus College</option>
												<option>Asnuntuck Community College</option>
												<option>Capital Community College</option>
												<option>Central Connecticut State University</option>
												<option>Charter Oak State College</option>
												<option>Connecticut College</option>
												<option>Eastern Connecticut State University</option>
												<option>Fairfield University</option>
												<option>Gateway Community College</option>
												<option>Goodwin College</option>
												<option>Hartford Seminary</option>
												<option>Holy Apostles College & Seminary</option>
												<option>Housatonic Community College</option>
												<option>LymeAcademy College of Fine Arts</option>
												<option>Manchester Community College</option>
												<option>Middlesex Community College</option>
												<option>Mitchell College</option>
												<option>Naugatuck Valley Community College</option>
												<option>Northwestern Connecticut Community College</option>
												<option>Norwalk Community College</option>
												<option>Paier College of Art </option>
												<option>Post University</option>
												<option>Quinebaug Valley Community College</option>
												<option>Quinnipiac University</option>
												<option>Rensselaer at Hartford</option>
												<option>Sacred Heart University</option>
												<option>Sanford-Brown College</option>
												<option selected="selected">Southern Connecticut State University</option>
												<option>Three Rivers Community College</option>
												<option>Trinity College</option>
												<option>Tunxis Community College</option>
												<option>United States Coast Guard Academy</option>
												<option>University of Bridgeport</option>
												<option>University of Connecticut</option>
												<option>University of Hartford</option>
												<option>University of New Haven</option>
												<option>University of Saint Joseph</option>
												<option>Wesleyan University</option>
												<option>Western Connecticut State University</option>
												<option>Yale University</option>
											</select>
										</td>
									</tr>

								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>
							<table class="tinner">
								<tr>
									<td id="tdMajor1" class="mylbl">Major</td>
									<td id="tdMajor2">
										<input id="major" type="text" name="major" class="text" data-parsley-trigger="blur" required autosize/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table class="tinner">
								<tr>
									<td id="tdCitizen1" class="mylbl">US Citizen</td>
									<td id="tdCitizen2">
										<input type="radio" name="citizen" class="citizen" id="citYes" value="Yes" required/>
										<label for="citYes">Yes</label>
										<input type="radio" name="citizen" class="citizen" id="citNo" value="No" />
										<label for="citYes">No</label>
									</td>
									<td id="tdgradYear1" class="mylbl">Expected Graduation Date</td>
									<td id="tdgradYear2">
										<input id="gradYear" type="text" name="dept" class="text" placeholder="Month/YYYY" data-parsley-trigger="blur" required autosize/>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>
							<table class="tinner">
								<tr>
									<td id="tdproject1" class="mylbl2">Project Title
										<br />
										<br />
									</td>
									<td id="tdproject2">
										<textarea id="title" rows="2" cols="20" name="project" class="textarea" data-parsley-trigger="blur" required autosize></textarea>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>
							<table class="tinner">
								<tr>
									<td id="tdperiod1" class="mylbl">Proposed Performance Period</td>
									<td id="tdperiod2">
										<input id="dateTo" type="text" data-parsley-trigger="blur" required>
										<span>to</span>
										<input id="dateFrom" type="text" data-parsley-trigger="blur" required>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>
							<table class="tinner">
								<tr>
									<td id="tdadvisor1" class="mylbl">Advisor Name</td>
									<td id="tdadvisor2">
										<input id="advisorName" type="text" name="advName" class="text" data-parsley-trigger="blur" required autosize/>
									</td>
									<td id="tdemail1" class="mylbl">Advisor Email</td>
									<td id="tdemail2">
										<input id="advisorEmail" type="email" name="advEmail" class="text" data-parsley-trigger="blur" required autosize/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table class="tinner">
								<tr>
									<td id="tdbudget1" class="mylbl">Budget Requested</td>
									<td id="tdbudget2">
										<span class="currency">$</span>
										<input id="budget" type="text" name="budget" class="text" data-parsley-trigger="blur" required autosize/>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>
							<table class="tinner">
								<tr>
									<td id="tdaward1" class="mylbl2">Previous Space Grant Award(s)
										<p>(Please list all: include title and date)</p>
									</td>
									<td id="tdaward2">
										<textarea id="prevAward" rows="3" cols="20" name="award" class="textarea"></textarea>
									</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>
						<td>
							<table>
								<td id="tdURL1" class="tdURL">Attachment URL:</td>
								<td id="tdURL2">
									<input id="url" type="text" name="url" class="url" placeholder="http://" data-parsley-type="url" required data-parsley-trigger="blur" parsley-type="url" />
								</td>
							</table>
						</td>
					</tr>

					<tr>
						<td id="tdsubmit">
							<br />
							<input id="apply" type="submit" name="submit" value="Apply Now" class="btn btn-default validate" />
						</td>
					</tr>


				</table>
				<br />

			</form>
		</section>
		<footer>
			<div class="row-bot">
			</div>
		</footer>
	</div>
</div>  

<script src="//cdn-static.formisimo.com/tracking/js/tracking.js"></script>
<script src="//cdn-static.formisimo.com/tracking/js/conversion.js"></script>
  
</body>
<script type='text/javascript' src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
<script type='text/javascript' src="js/jquery-ui.min.js"></script>
<script type='text/javascript' src="js/parsley.min.js"></script>
<script type="text/javascript">
    // Add form validation
    $('#form').parsley({
    	errorsWrapper: '',
    	errorTemplate: ''
    });

    // Add date picker to form
    $('#dateTo, #dateFrom').datepicker();
    // $('#gradYear').datepicker({
    //     blurMonth: true,
    //     blurYear: true,
    //     showButtonPanel: true,
    //     dateFormat: 'MM yy',
    //     onClose: function(dateText, inst) {
    //         var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
    //         var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
    //         $(this).datepicker('setDate', new Date(year, month, 1));
    //     }
    // });

$(document).ready(function() {
	$('form').submit(function(event) {

		var grantId = $('.grant:checked').val();
		var first = $("#first").val();
		var last = $("#last").val();
		var mi = $("#mi").val();
		var uniName = $("#uniName").val();
		var gradYear = $("#gradYear").val();
		var citizen = $('.citizen:checked').val();
		var major = $("#major").val();
		var prevAward = $("#prevAward").val();
		var dateTo = $("#dateTo").val();
		var dateFrom = $("#dateFrom").val();
		var title = $("#title").val();
		var budget = $("#budget").val();
		var advisor = $("#advisorName").val();
		var advisorEmail = $("#advisorEmail").val();
		var url = $("#url").val();

		var formdata = JSON.stringify({
			"grantId": grantId,
			"first": first,
			"last": last,
			"middleInitial": mi,
			"uniName": uniName,
			"gradYear": gradYear,
			"Daterange": dateTo + " - " + dateFrom,
			"major": major,
			"citizen": citizen,
			"prevAward": prevAward,
			"title": title,
			"budget": budget,
			"advisor": advisor,
			"advisorEmail": advisorEmail,
			"url": url
		});

		console.log(formdata);
		response = $.ajax({
			url: "http://ctspace.me/post/grant/applicant/student",
			data: formdata,
			type: "PUT",
			dataType: 'json',
			cache: false,
			contentType: "application/json",
			complete: function(response) {
				console.log(response);
				console.log(response.responseText);
				alert(response.responseText);
			}
		});
		event.preventDefault()
	});

});
</script>

<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o),
		m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', 'http://www.google-analytics.com/analytics.js', 'ga');
	ga('create', 'UA-61427249-1', 'auto');
	ga('send', 'pageview');
</script>
<!-- <script src="http://cdn-static.formisimo.com/tracking/js/conversion.js"></script> -->

<script type="text/javascript">Cufon.now();</script>
</html>

<?php
}else{
	header('Location: form.php?err=2');
}
?>