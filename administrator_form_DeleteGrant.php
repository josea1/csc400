<?php
	error_reporting(E_ALL);
	ini_set('display_errors',1);

		//set timezone for correct timestamp		
		date_default_timezone_set('America/New_York');

		 if(empty($_POST['deleteName'])){
					echo 'Please enter a name';
					echo "<a href=adminDel.html>Click Return</a>";
					die();
				}

		       $grant = array(
									"GrantName"         =>    $_POST['deleteName'],
									"Timestamp"     	=>    date('m-d-Y H:i:s'),
					   );

					$json = json_encode($grant, JSON_PRETTY_PRINT);

					printf("<pre><font size=8>%s</font></pre>", $json);			

					// php5-curl
                    $url = "ctspace.me/delete/grant";                        
					$ch = curl_init($url);                                                                      
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
					curl_setopt($ch, CURLOPT_POSTFIELDS, $json);                                                                  
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
					// curl_setopt($ch, CURLOPT_PORT, 5001);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 100);      
                    curl_setopt($ch, CURLOPT_HEADER, 1); 
                    curl_setopt($ch, CURLINFO_HEADER_OUT, true);                                                              
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
					    'Content-Type: application/json',                                                                                
					    'Content-Length: ' . strlen($json))                                                                       
					);                                                                                                                   

					if(curl_exec($ch) === false){
                        echo '<font size=15><strong>DEBUG MODE</strong>  -  <a href=adminBadSubmit.html>What the user normally sees</a></font><br>';
                        echo '<font size=15>Error: ' . curl_error($ch) . '<br>';
                    }
                    else{    
                        echo '<font size=15><strong>DEBUG MODE</strong>  -  <a href=adminSubmit.html>What the user normally sees</a></font><br>';                                                          
                        echo '<font size=15>JSON successfully sent<br>'; 
					}
														  		
                    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    echo 'http code: ' . $httpcode .'<br>';
                    var_dump(curl_getinfo($ch,CURLINFO_HEADER_OUT)); 
                    echo '</font>';

        if(curl_exec($ch) === false){
               header('Location:adminBadSubmit.html');     
        }
        else{                                                              
            header('Location:adminSubmit.html');            
        }

    curl_close($ch);
    

?>